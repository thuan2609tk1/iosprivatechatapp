//
//  StorageManager.swift
//  Private Chat
//
//  Created by Thuận Nguyễn Văn on 20/02/2021.
//

import Foundation
import Firebase

final class StorageManager {
    
    static let shared = StorageManager()
    
    private let storage = Storage.storage().reference()
    
    /*
     /image/thuan-gmail-com_profile_picture.png
     */
    
    public typealias UploadPictureCompletion = (Result<String, Error>) -> Void
    
    ///Upload picture to firebase storage and returns completion with url string to dowload
    public func  uploadProfilePicture(with data: Data, fileName: String, completion: @escaping UploadPictureCompletion) {
        storage.child("images/\(fileName)").putData(data, metadata: nil) { (metaData, error) in
            guard error == nil else {
                //failed
                print("failed to upload data to firebase for profile picture")
                completion(.failure(StorageErrors.failToUpload))
                return
            }
            
            self.storage.child("images/\(fileName)").downloadURL { (url, error) in
                guard let url = url else {
                    print("Failed to get dowload url")
                    completion(.failure(StorageErrors.failToGetDowloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("Dowload Url returned: \(urlString)")
                completion(.success(urlString))
            }
        }
    }
    
    public enum StorageErrors: Error {
        case failToUpload
        case failToGetDowloadUrl
    }
    
    public func dowloadURL(for path: String, completion: @escaping (Result<URL, Error>) -> Void) {        
        let reference = self.storage.child(path)
        reference.downloadURL { (url, error) in
            guard let url = url , error == nil else {
                completion(.failure(StorageErrors.failToGetDowloadUrl))
                return
            }
            completion(.success(url))
        }
    }
}

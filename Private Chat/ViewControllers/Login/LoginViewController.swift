//
//  LoginViewController.swift
//  Private Chat
//
//  Created by Thuận Nguyễn Văn on 15/02/2021.
//

import UIKit
import Firebase
import FBSDKLoginKit
import GoogleSignIn
import JGProgressHUD

class LoginViewController: UIViewController {
    
    private let spinner = JGProgressHUD(style: .dark)
    
    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.clipsToBounds = true
        return scrollView
    }()
    
    
    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "chat")
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let emailField: UITextField = {
        let field = UITextField()
        field.autocapitalizationType = .none
        field.autocorrectionType = .no
        field.returnKeyType = .continue
        field.layer.cornerRadius = 5
        field.layer.borderWidth = 1.0
        field.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        field.placeholder = "Email Address..."
        field.leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 5.0, height: 0.0))
        field.leftViewMode = .always
        return field
    }()
    
    private let passwordField: UITextField = {
        let field = UITextField()
        field.autocapitalizationType = .none
        field.autocorrectionType = .no
        field.returnKeyType = .done
        field.layer.cornerRadius = 5
        field.layer.borderWidth = 1.0
        field.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        field.placeholder = "Password..."
        field.leftView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 5.0, height: 0.0))
        field.leftViewMode = .always
        field.isSecureTextEntry = true
        return field
    }()
    
    private let loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("Login", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .green
        return button
    }()
    
    private let facebookLoginButton: FBLoginButton = {
        let button = FBLoginButton()
        button.permissions = ["public_profile", "email"]
        return button
    }()
    
    private let googleSigInButton: GIDSignInButton = {
        let button = GIDSignInButton()
        return button
    }()
    
    private var loginObserver: NSObjectProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Login"
        view.backgroundColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Register", style: .done, target: self, action: #selector(didTapRegister))
        loginButton.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
        emailField.delegate = self
        passwordField.delegate = self
        facebookLoginButton.delegate = self
        
        NotificationCenter.default.addObserver(forName: .didLogInNotification, object: nil, queue: .main) { [weak self](_) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        }
        GIDSignIn.sharedInstance()?.presentingViewController = self
        //Add Subview
        addSubView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollView.frame = view.bounds
        let size = scrollView.width / 3
        imageView.frame = CGRect(x: (scrollView.width - size)/2, y: 60.0, width: size, height: size)
        emailField.frame = CGRect(x: 10.0, y: imageView.bottom + 10.0, width: scrollView.width - 20.0, height: 50.0)
        passwordField.frame = CGRect(x: 10.0, y: emailField.bottom + 10.0, width: scrollView.width - 20.0, height: 50.0)
        loginButton.frame = CGRect(x: 10.0, y: passwordField.bottom + 10.0, width: scrollView.width - 20.0, height: 50.0)
        facebookLoginButton.frame = CGRect(x: 10.0, y: loginButton.bottom + 10.0, width: scrollView.width - 20.0, height: 50.0)
        googleSigInButton.frame = CGRect(x: 10.0, y: facebookLoginButton .bottom + 10.0, width: scrollView.width - 20.0, height: 50.0)
    }
    
    private func addSubView(){
        self.view.addSubview(scrollView)
        self.scrollView.addSubview(imageView)
        self.scrollView.addSubview(emailField)
        self.scrollView.addSubview(passwordField)
        self.scrollView.addSubview(loginButton)
        self.scrollView.addSubview(facebookLoginButton)
        self.scrollView.addSubview(googleSigInButton)
    }
    
    deinit {
        if let observer = loginObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
    @objc private func didTapLogin() {
        emailField.resignFirstResponder()
        passwordField.resignFirstResponder()
        guard let email = emailField.text, let password = passwordField.text, !email.isEmpty, !password.isEmpty, password.count >= 6 else {
            alertUserLoginError()
            return
        }
        spinner.show(in: view)
        
        //Firebase Login:
        Firebase.Auth.auth().signIn(withEmail: email, password: password) { [weak self](authResult, error) in
            
            guard let strongSelf = self else{
                return
            }
            
            DispatchQueue.main.async {
                strongSelf.spinner.dismiss()
            }
            
            guard let result = authResult, error == nil else {
                print("Failed to loged in with: \(email)")
                return
            }
            let user = result.user
            UserDefaults.standard.set(email, forKey: "email")
            print("Loged in with: \(user)")
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        }
    }
    
    private func alertUserLoginError() {
        let alert = UIAlertController(title: "Woops", message: "Please enter all information to login", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    @objc private func didTapRegister() {
        let vc = RegisterViewController()
        vc.title = "Create Account"
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailField {
            passwordField.becomeFirstResponder()
        }
        if textField == passwordField {
            didTapLogin()
        }
        return true
    }
}

extension LoginViewController: LoginButtonDelegate {
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        // No Operation
    }
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        guard  let token = result?.token?.tokenString else {
            print("User Failed to login with Facebook")
            return
        }

        
        let facebookRequest = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "email, first_name, last_name, picture.type(large)"], tokenString: token, version: nil, httpMethod: .get)
        
        facebookRequest.start { (_, result, error) in
            guard let result = result as? [String: Any], error == nil else {
                print("Failed to make facebook graph request")
                return
            }
            print("Fabebook Response Result: \(result)")
            guard let firstName = result["first_name"] as? String,
                  let lastName = result["last_name"] as? String,
                  let email = result["email"] as? String,
                  let picture = result["picture"] as? [String: Any],
                  let data = picture["data"] as? [String: Any],
                  let pictureUrl = data["url"] as? String
            else {
                print("Failed to get username and email from facebook result")
                return
            }
            /*
             Result: ["first_name": Thuận, "last_name": Nguyễn, "picture": {
                 data =     {
                     height = 200;
                     "is_silhouette" = 0;
                     url = "https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2921822584807666&height=200&width=200&ext=1616381172&hash=AeTOZpgWCTyOh66grUU";
                     width = 200;
                 };
             */
            UserDefaults.standard.set(email, forKey: "email")
            DatabaseManager.shared.userExists(with: email) { (exists) in
                if !exists {
                    let chatUser = ChatAppUser(firstName: firstName, lastName: lastName, emailAddress: email)
                    DatabaseManager.shared.insertUser(with: chatUser) { (success) in
                        if success {
                            //Upload Image:
                            guard let url = URL(string: pictureUrl) else {
                                return
                            }
                            print("Dowloading data from Facebook image....")
                            URLSession.shared.dataTask(with: url) { (data, _, _) in
                                guard let data = data else {
                                    print("Failed to get Data From Fabebook")
                                    return
                                }
                                print("Got data from Facebook, uploading....")
                                let fileName = chatUser.profilePictureFileName
                                StorageManager.shared.uploadProfilePicture(with: data, fileName: fileName) { (result) in
                                    switch result {
                                    case .success(let dowloadUrl):
                                        UserDefaults.standard.setValue(dowloadUrl, forKey: "profile_picture_url")
                                        print(dowloadUrl)
                                    case .failure(let error):
                                        print("Storage failed: \(error)")

                                    }
                                }
                            }.resume()
                        }
                    }
                }
            }
        }

        let credential = FacebookAuthProvider.credential(withAccessToken: token)
        Firebase.Auth.auth().signIn(with: credential) { [weak self](authResult, error) in
            guard let strongSelf = self else {
                return
            }
            guard authResult != nil, error == nil else {
                print("Facebook Credential Fiale")
                return
            }
            print("Successfully logged user in")
            strongSelf.navigationController?.dismiss(animated: true, completion: nil)
        }
        
    }
    
    
}
